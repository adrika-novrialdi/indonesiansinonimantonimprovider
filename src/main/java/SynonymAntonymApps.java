import service.grammar.indonesian.IndonesianThesaurusHelper;
import service.grammar.ThesaurusHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SynonymAntonymApps {
    private static final String FAILED_RESPONSE = "Tidak ditemukan";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String userCommand = reader.readLine();
            userCommand = userCommand.replace(" ", "");
            processInput(userCommand);
        }



    }

    private static void processInput(String userCommand) {
        ThesaurusHelper thesaurusHelper = IndonesianThesaurusHelper.getInstance();
        if (userCommand.equalsIgnoreCase("EXIT")) {
            System.exit(0);
        }
        System.out.println(findSynonym(userCommand, thesaurusHelper));

        System.out.println(findAntonym(userCommand, thesaurusHelper));
    }

    private static String findAntonym(String userCommand, ThesaurusHelper thesaurusHelper) {
        String responseAntonym = thesaurusHelper.findAntonym(userCommand);

        if (!responseAntonym.equalsIgnoreCase(FAILED_RESPONSE)) {
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("Antonim : %s ", responseAntonym));
            return builder.toString();
        }
        return "";
    }

    private static String findSynonym(String userCommand, ThesaurusHelper thesaurusHelper) {
        String responseSynonym = thesaurusHelper.findSynonym(userCommand);
        if (!responseSynonym.equalsIgnoreCase(FAILED_RESPONSE)) {
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("Sinonim : %s", responseSynonym));
            return builder.toString();
        } else {
            return "Sinonim " +FAILED_RESPONSE;
        }
    }
}
