package factory;

import service.grammar.AntonymHelper;
import service.grammar.SynonymHelper;
import service.grammar.indonesian.IndonesianAntonymHelper;
import service.grammar.indonesian.IndonesianSynonymHelper;

public class IndonesianGrammarHelperFactory  implements GrammarHelperFactory {
    @Override
    public SynonymHelper createSynonymHelper() {
        return new IndonesianSynonymHelper();
    }

    @Override
    public AntonymHelper createAntonymHelper() {
        return new IndonesianAntonymHelper();
    }
}
