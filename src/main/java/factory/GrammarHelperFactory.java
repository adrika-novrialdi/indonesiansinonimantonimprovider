package factory;

import service.grammar.AntonymHelper;
import service.grammar.SynonymHelper;

public interface GrammarHelperFactory {
    public SynonymHelper createSynonymHelper();
    public AntonymHelper createAntonymHelper();
}
