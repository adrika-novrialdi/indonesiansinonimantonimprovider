package service.grammar;

public interface SynonymHelper {
    public String findSynonym(String words);
}
