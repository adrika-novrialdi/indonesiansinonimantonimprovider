package service.grammar;

import factory.GrammarHelperFactory;


public abstract class ThesaurusHelper {
    GrammarHelperFactory grammarHelperFactory;
    private AntonymHelper antonymHelper;
    private SynonymHelper synonymHelper;

    public ThesaurusHelper(GrammarHelperFactory grammarHelperFactory) {
        this.grammarHelperFactory = grammarHelperFactory;
        initializeClass();
    }

    private void initializeClass() {
        synonymHelper = grammarHelperFactory.createSynonymHelper();
        antonymHelper = grammarHelperFactory.createAntonymHelper();
    }

    public String findSynonym(String word) {
        return synonymHelper.findSynonym(word);
    }

    public String findAntonym(String word) {
        return antonymHelper.findAntonym(word);
    }
}
