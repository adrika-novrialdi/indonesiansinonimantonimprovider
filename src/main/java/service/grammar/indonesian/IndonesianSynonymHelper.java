package service.grammar.indonesian;

import service.grammar.SynonymHelper;

import java.io.IOException;

public class IndonesianSynonymHelper implements SynonymHelper {
    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private String responseToInput = "";
    IndonesianThesaurusParser parser = IndonesianThesaurusParser.getInstance();

    @Override
    public String findSynonym(String word) {
        try {
            responseToInput = parser.getContentData(word, "sinonim");
        }catch (IOException e) {
            responseToInput = FAILED_RESPONSE;
        }
        return responseToInput;
    }
}
