package service.grammar.indonesian;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;


import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class IndonesianThesaurusParser {
        private final String DEFAULT_URL = "http://www.sinonimkata.com/";
        private  final String FAILED_RESPONSE = "Tidak ditemukan";
        private static WebClient webClient;
        private static boolean isInitialize = false;
        private static HtmlPage pageResponse;
        private static IndonesianThesaurusParser parser = new IndonesianThesaurusParser();

        private IndonesianThesaurusParser() {
        }

        public static IndonesianThesaurusParser getInstance() {
            return parser;
        }

        private HtmlPage getResponsePage() throws IOException {
            if (!isInitialize) {
                initialize();
            }
            return pageResponse;
        }

        public String getContentData(String word, String query) throws IOException {
            return createResponse(getResponseData(word), query);
        }

        private void initialize() throws IOException {
            webClient = new WebClient(BrowserVersion.CHROME);
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            pageResponse = webClient.getPage(DEFAULT_URL);
            isInitialize = true;
        }

        private DomNodeList<DomElement> getResponseData(String word) throws IOException {
            HtmlPage page = getResponsePage();
            HtmlForm searchForm = page.getHtmlElementById("form_search_submit");
            HtmlTextInput nameInput = page.getHtmlElementById("form_search_text");
            nameInput.setValueAttribute(word);

            HtmlSubmitInput input = searchForm.getInputByValue("Cari");
            HtmlPage newPage = input.click();



            return newPage.getElementsByTagName("td");
        }

        private  String createResponse(DomNodeList<DomElement> responseData, String responseQuery) {
            String responseToInput;
            int startingIndex = 0;
            boolean isFound = false;
            for (int searchIndex = 0; searchIndex < responseData.size(); searchIndex++) {
                String responseHeader = responseData.get(searchIndex).asText();
                responseHeader =  responseHeader.replace(" ", "");
                if (responseHeader.equalsIgnoreCase(responseQuery)) {
                    startingIndex = searchIndex;
                    isFound = true;
                }
            }
            if (isFound) {
                String rawResponse = responseData.get(startingIndex+2).asText();
                responseToInput = cleanResponse(rawResponse);
            } else {
                responseToInput = FAILED_RESPONSE;
            }
            return responseToInput;
        }

        private String cleanResponse(String rawResponse) {
            LinkedList<String> rawResponseAsList = new LinkedList<>(Arrays.asList(rawResponse.split("")));
            rawResponseAsList.remove(rawResponseAsList.size()-1);
            return makeStringResponseFromList(rawResponseAsList);

        }

        private  String makeStringResponseFromList(List<String> rawResponseAsList) {
            StringBuilder responseBuilder = new StringBuilder();
            for (String words : rawResponseAsList) {
                responseBuilder.append(words);
            }
            return responseBuilder.toString();
        }
}
