package service.grammar.indonesian;

import factory.IndonesianGrammarHelperFactory;
import service.grammar.ThesaurusHelper;

public class IndonesianThesaurusHelper extends ThesaurusHelper {
    private static IndonesianThesaurusHelper indonesianThesaurusHelper = new IndonesianThesaurusHelper();
    private IndonesianThesaurusHelper() {
        super(new IndonesianGrammarHelperFactory());
    }

    public static IndonesianThesaurusHelper getInstance() {
        return indonesianThesaurusHelper;
    }
}
