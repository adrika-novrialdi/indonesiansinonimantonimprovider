package service.grammar.indonesian;

import service.grammar.AntonymHelper;

import java.io.IOException;

public class IndonesianAntonymHelper implements AntonymHelper {
    private final String FAILED_RESPONSE = "Tidak ditemukan";
    private String responseToInput = "";
    IndonesianThesaurusParser parser = IndonesianThesaurusParser.getInstance();
    @Override
    public String findAntonym(String word) {
        try {

            responseToInput = parser.getContentData(word, "antonim");
        }catch (IOException e) {
            responseToInput = FAILED_RESPONSE;
        }
        return responseToInput;
    }
}
