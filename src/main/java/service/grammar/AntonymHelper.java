package service.grammar;

public interface AntonymHelper {
    public String findAntonym(String word);
}
